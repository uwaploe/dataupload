package uploader

import (
	"flag"
	"reflect"
	"testing"
)

func TestCmdLine(t *testing.T) {
	table := []struct {
		in  []string
		out appEnv
		err error
	}{
		{
			in:  []string{"--version"},
			out: appEnv{},
			err: versReq,
		},
		{
			in:  []string{"-help"},
			out: appEnv{},
			err: flag.ErrHelp,
		},
		{
			in: []string{"--key", "keyfilename", "sftp://user@host/some/path", "srcdir"},
			out: appEnv{srcDir: "srcdir", dstDir: "some/path", user: "user",
				server: "host:22", keyFile: "keyfilename"},
		},
		{
			in: []string{"sftp://user@host:2022/some/path", "srcdir"},
			out: appEnv{srcDir: "srcdir", dstDir: "some/path", user: "user",
				server: "host:2022"},
		},
	}

	for _, e := range table {
		var app appEnv
		err := app.fromArgs(e.in)
		if err != e.err {
			t.Fatal(err)
		}
		app.fl = nil
		if !reflect.DeepEqual(app, e.out) {
			t.Errorf("Bad value; expected %#v, got %#v",
				e.out, app)
		}

	}
}
