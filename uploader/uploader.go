// Package uploads files using SFTP and removes them from the local system.
package uploader

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/url"
	"os"
	"os/user"
	"strings"

	"github.com/pkg/sftp"
)

const Usage = `Usage: dataupload [options] sftp://HOST/PATH srcdir

Upload the contents of SRCDIR to an sftp server.
`

type appEnv struct {
	srcDir    string
	dstDir    string
	server    string
	user      string
	keyFile   string
	keepFiles bool
	fl        *flag.FlagSet
}

var versReq = errors.New("show version")

func (app *appEnv) fromArgs(args []string) error {
	fl := flag.NewFlagSet("uploader", flag.ContinueOnError)
	fl.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		fl.PrintDefaults()
	}

	showVers := fl.Bool("version", false, "Show program version information and exit")
	fl.StringVar(&app.keyFile, "key", "", "SSH private key file")
	fl.BoolVar(&app.keepFiles, "keep", false, "If true, keep srcdir files")

	app.fl = fl
	if err := fl.Parse(args); err != nil {
		return err
	}

	if *showVers {
		return versReq
	}

	req := fl.Args()
	if len(req) < 2 {
		return errors.New("Missing command-line arguments")
	}

	u, err := url.Parse(req[0])
	if err != nil {
		return fmt.Errorf("URL parse error %q: %w", req[0], err)
	}

	if app.user = u.User.Username(); app.user == "" {
		user, err := user.Current()
		if err != nil {
			return fmt.Errorf("Cannot lookup user name: %w", err)
		}
		app.user = user.Username
	}

	if strings.Index(u.Host, ":") != -1 {
		app.server = u.Host
	} else {
		app.server = u.Host + ":22"
	}
	app.dstDir = u.Path[1:]
	app.srcDir = req[1]

	return nil
}

func (app *appEnv) run(ctx context.Context) error {
	conn, err := pubkeyConnect(app.user, app.server, app.keyFile)
	if err != nil {
		return err
	}
	defer conn.Close()

	c, err := sftp.NewClient(conn)
	if err != nil {
		return err
	}
	defer c.Close()

	n, err := uploadDirFiles(ctx, c, app.srcDir, app.dstDir, app.keepFiles)
	log.Printf("Uploaded %d files", n)

	return err
}

func CLI(ctx context.Context, vers string, args []string) int {
	var app appEnv
	err := app.fromArgs(args)
	if err != nil {
		switch err {
		case versReq:
			fmt.Fprintf(os.Stderr, "%s\n", vers)
		}

		return 2
	}
	if err = app.run(ctx); err != nil {
		fmt.Fprintf(os.Stderr, "Runtime error: %v\n", err)
		return 1
	}
	return 0
}
