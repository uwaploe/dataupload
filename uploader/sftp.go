package uploader

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	"github.com/pkg/sftp"
)

func uploadFile(c *sftp.Client, srcPath, dstDir string) (int64, error) {
	fin, err := os.Open(srcPath)
	if err != nil {
		return 0, err
	}
	defer fin.Close()

	filename := filepath.Base(srcPath)
	fout, err := c.Create(c.Join(dstDir, filename))
	if err != nil {
		return 0, err
	}
	defer fout.Close()

	return fout.ReadFrom(fin)
}

func openDir(dir string) (chan os.FileInfo, error) {
	d, err := os.Open(dir)
	if err != nil {
		return nil, err
	}

	ch := make(chan os.FileInfo, 1)
	go func() {
		defer close(ch)
		defer d.Close()

		for {
			fi, err := d.Readdir(1)
			if err != nil {
				return
			}
			ch <- fi[0]
		}
	}()

	return ch, nil
}

func uploadDirFiles(ctx context.Context, c *sftp.Client,
	srcDir, dstDir string, keep bool) (int, error) {
	var count int

	ch, err := openDir(srcDir)
	if err != nil {
		return count, err
	}

	for fi := range ch {
		select {
		case <-ctx.Done():
			return count, ctx.Err()
		default:
		}

		if fi.IsDir() {
			continue
		}
		path := filepath.Join(srcDir, fi.Name())
		_, err = uploadFile(c, path, dstDir)
		if err != nil {
			return count, fmt.Errorf("Upload failed %q: %w", path, err)
		}
		count++

		if !keep {
			err = os.Remove(path)
			if err != nil {
				return count, fmt.Errorf("Cannot remove %q: %w", path, err)
			}
		}
	}

	return count, nil
}
