module bitbucket.org/uwaploe/dataupload

go 1.13

require (
	github.com/pkg/errors v0.9.1
	github.com/pkg/sftp v1.11.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)
