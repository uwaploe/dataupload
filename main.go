// Upload files using SFTP to a remote server and remove them from the local system
package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/uwaploe/dataupload/uploader"
)

var Version = "dev"
var BuildDate = "unknown"

func main() {
	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Cancel the context on a signal interrupt
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			cancel()
		}
	}()

	os.Exit(uploader.CLI(ctx, Version, os.Args[1:]))
}
